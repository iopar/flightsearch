const HtmlWebpackPlugin = require('html-webpack-plugin');
const ScriptExtPlugin = require('script-ext-html-webpack-plugin');
const { AngularCompilerPlugin } = require('@ngtools/webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
var fs = require("fs");

module.exports = function(){
    return {
        mode: 'development',
        entry: ['./src/main.ts', './src/assets/styles/style.scss'],
        output: {
            path: __dirname + '/dist',
            filename: 'app.js'
        },
        resolve: {
            extensions: ['.ts', '.js']
        },
        node: {
            fs: "empty"
        },
        module: {
            rules: [
                { test: /\.ts$/, loader: '@ngtools/webpack'},
                { test: /\.css$/, loader: 'raw-loader' },
                { test: /\.html$/, loader: 'raw-loader' },
                {
                    test: /\.(ttf|eot|woff|woff2)$/,
                    use: {
                        loader: "file-loader",
                        options: {
                            name: "assets/styles/fonts/[name].[ext]",
                        }
                    }
                },
                {
                    test: /\.scss$/,
                    use:[
                        {
                            loader: 'file-loader',
                            options: {
                                name: 'assets/styles/[name].css',
                            }
                        },
                        {
                            loader: 'extract-loader'
                        },
                        {
                            loader: 'css-loader?-url'
                        },
                        {
                            loader: 'sass-loader'
                        }
                    ]
                },
                {
                    // Mark files inside `@angular/core` as using SystemJS style dynamic imports.
                    // Removing this will cause deprecation warnings to appear.
                    test: /[\/\\]@angular[\/\\]core[\/\\].+\.js$/,
                    parser: { system: true },  // enable SystemJS
                }
            ]
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: __dirname + '/index.html',
                output: __dirname + '/dist',
                inject: 'head'
            }),
            new CopyWebpackPlugin([
                {from:'src/assets/styles/fonts',to:'assets/styles/fonts'}
            ]),
            new ScriptExtPlugin({
                defaultAttribute: 'defer'
            }),

            new AngularCompilerPlugin({
                tsConfigPath: './tsconfig.json',
                entryModule: './src/app/app.module#AppModule',
                sourceMap: true
            })
        ]
    }
};