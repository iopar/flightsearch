import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FlightsModule } from './flights/flights.module';
@NgModule({
    bootstrap: [ AppComponent ],
    declarations: [
        AppComponent
    ],
    imports: [
        FlightsModule
    ]
})

export class AppModule{}