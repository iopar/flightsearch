import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, EMPTY } from "rxjs";
import { expand } from "rxjs/operators";

@Injectable()
export class OffersService {
    constructor(private http: HttpClient) {}

    /**
    * @param guid - random guid for each individual call which serves as an unique search identifier
    */
    queryFlights(guid:String): Observable<any[]> {
        let safetyEscape = 30; // this is the ammount of time we are allowed to call the method recursively, so we don't end up in an infinite loop if the backend fails

        const getFlights = (): Observable<any> => {
            return this.http.get(`http://0.0.0.0:8080/http://momondodevapi.herokuapp.com/api/1.0/FlightSearch/${guid}`);
        };

        return getFlights()
            .pipe(
                expand((res: Response) => {
                    safetyEscape--;
                    if (!res['Done'] && safetyEscape !== 0 ) {
                        return getFlights();
                    } else {
                        return EMPTY;
                    }
                })
            )
    }
}