import { NgModule } from '@angular/core';

// Pipes
import { Min2duration  } from './dates/min2duration';
import { PriceFormat  } from './prices/priceformat';

@NgModule({
  declarations: [
    Min2duration,
    PriceFormat
  ],
  imports: [],
  exports: [
    Min2duration,
    PriceFormat
  ]
})
export class PipesModule {}