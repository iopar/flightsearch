import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'priceformat',
})
export class PriceFormat implements PipeTransform {
    transform(value: number) {
        // Rounding the price is not a good idea at all. the only reason this is added is because of
        // values that end in .00004
        return Math.round(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
}