import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'min2duration',
})
export class Min2duration implements PipeTransform {
    transform(input: number) {
        let hrs = Math.floor(input / 60),
            min = (input % 60);
        return hrs + "h " + min + "m";
    }
}