import { Component, Input } from '@angular/core';

@Component({
    selector: "segment",
    templateUrl: `segment.component.html`
})


export class SegmentComponent {
    @Input() segment;
}
