import { NgModule } from "@angular/core";
import { FlightsComponent } from "@core/flights/flights.component";
import { SegmentComponent } from "@core/segment/segment.component";
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { OffersService } from '@core/services/offers.service';
import { PipesModule } from '@core/pipes/pipes.module'

@NgModule({
    declarations: [
        FlightsComponent,
        SegmentComponent
    ],
    exports: [ FlightsComponent ],
    imports: [
        BrowserModule,
        HttpClientModule,
        PipesModule
    ],
    providers: [
        OffersService,
        HttpClient
    ]
})

export class FlightsModule{
}