import { Component, OnInit } from '@angular/core';
import { OffersService } from '@core/services/offers.service';
@Component({
    selector: 'flights',
    templateUrl: `flights.component.html`
})

export class FlightsComponent implements OnInit{
    constructor( private OffersService: OffersService){}

    public flightsData : {}

    public ngOnInit(): void {
        let guid = this.generateGUID(),
            flightsData = {
                Offers: [],
                Legs: [],
                Segments: [],
                Flights: []
            }
        this.OffersService.queryFlights(guid).subscribe( res => {
            //Since the data comes in multiple payloads, we need to merge it together
            // before defining the component data
            flightsData =  this.compileFlightsData(flightsData, res);

            // When the last call is done, we can update the component data
            if(res['Done']){
                this.mapFlightsData(flightsData)
            }
        })
    }

    /**
     *
     * @param initialData - Initial data is the previously compiled data, to which we need to concat the new data
     * @param rawData - New raw data, retrieved directly from the api
     */
    compileFlightsData(initialData, rawData){
        initialData.Offers = initialData.Offers.concat(rawData['Offers']);
        initialData.Legs = initialData.Legs.concat(rawData['Legs']);
        initialData.Segments = initialData.Segments.concat(rawData['Segments']);
        initialData.Flights = initialData.Flights.concat(rawData['Flights']);

        return initialData;
    }

    /**
     * Doesn't return anything, but sets the component output variable
     * @param data  - collection of compiled data retrieved from the api
     */
    mapFlightsData(data):void{
        let flightsArr = [];

        // The offers are the display data, so that's the first level we loop
        data.Offers.forEach( o => {
            let segments = [],
                duration = 0;
            // each offer presents a flight index. Based on that, we can retrieve the segment indexs from the flights array
            data.Flights[o.FlightIndex].SegmentIndexes.forEach(sIndex => {
                duration = data.Segments[sIndex].Duration;
                // From the segments array, we need to retrieve the legs (which represent the ammount of individual trips).
                // This contains all the information we need for the display - Airline name, origin and destination citites, etc.
                data.Segments[sIndex].LegIndexes.forEach( lIndex => {
                    let segmentLeg = data.Legs[lIndex];
                    segments.push(segmentLeg);
                })
            });

            // Compile an object from the data above, so we map it in the format we need
            let offerData = {
                price: o.Price,
                deeplink: o.Deeplink,
                currency: o.Currency,
                duration,
                segments
            };
            flightsArr.push(offerData);
        });

        // sort the offers array ascending based on offer price
        this.flightsData = flightsArr.sort(function(a, b){return a.price-b.price});
    }

    /**
     * Returns a random hexidecimal string of form 17f3c-10fe7-19621-17b13-10729
     */
    generateGUID(): string {
        function s5() {
          return Math.floor((1 + Math.random()) * 0x10000).toString(16);
        }
        return `${s5()}-${s5()}-${s5()}-${s5()}-${s5()}`;
    }
    /**
     *
     * @param url - The url where we need to open (we could add campaign data and other parameters depnding on where it's used)
     */
    redirectToVendor(url: string): void{
        window.open(url);
    }

}