# README #

##Dependencies##

- node@8 (minimum)
- yarn/npm

### What is this repository for? ###

* This is an exercise displaying flights data from a mock api

### How do I get set up? ###

* clone the repository locally - `git clone https://iopar@bitbucket.org/iopar/flightsearch.git`
* run `yarn install`
* run `yarn deploy` - this will build the assets folder and start the express server at http://localhost:3000`
* How to run tests - `yarn test`
