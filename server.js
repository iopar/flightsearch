const express = require('express');
const cors_proxy = require('cors-anywhere');

const port = 3000 || process.env.PORT;
const corsPort = 8080 || process.env.PORT;
const app = express();
// Listen on a specific host via the HOST environment variable
const host = process.env.HOST || '0.0.0.0';


app.engine('html', require('ejs').renderFile);

app.set('view engine', 'html');
app.set('views', 'dist');

app.use('/', express.static('dist', { index: false }));
app.use('/node_modules', express.static('node_modules', { index: false }));

app.get('/*', (req, res) => {
    res.render('./index', {req, res});
});

app.listen(port, () => {
        console.log(`Live on: http://localhost:${port}`);
})



cors_proxy.createServer({
    originWhitelist: [], // Allow all origins
    requireHeader: ['origin', 'x-requested-with'],
    removeHeaders: ['cookie', 'cookie2']
}).listen(corsPort, host, function() {
    console.log('Running CORS Anywhere on ' + host + ':' + corsPort);
});