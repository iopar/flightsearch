import { SegmentComponent } from '@core/segment/segment.component';
import { async, TestBed, inject } from '@angular/core/testing';

describe('SegmentComponent', () => {
    let component: SegmentComponent;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ SegmentComponent ]
        });
    }));
    test('should exist', () => {
        inject([],() =>{
            expect(component).toBeDefined();
        });
    });
});