import { async, TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { FlightsComponent } from '@core/flights/flights.component';
import { OffersService } from '@core/services/offers.service';

describe('FlightsComponent', () => {
    let component: FlightsComponent;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ HttpClientTestingModule ],
            providers: [ OffersService ],
            declarations: [ FlightsComponent ]
        });
    }));

    test('should exist', () => {
        inject(
        [HttpTestingController, OffersService],
        (httpMock: HttpTestingController, dataService: OffersService) =>{
            expect(component).toBeDefined();
        });
    });
});